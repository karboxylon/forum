<?php

namespace app\components\votewidget;

use app\models\interfaces\VoteInterface;
use Yii;
use yii\base\Widget;

class VoteWidget extends Widget
{
    /** @var  VoteInterface */
    public $model;

    /**
     * @var int
     */
    public $forumElementType;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $userVote = $this->model->getUserVote(Yii::$app->user->id);

        return $this->render(
            '_vote',
            [
                'model' => $this->model,
                'userVote' => $userVote,
                'forumElementType' => $this->forumElementType
            ]);
    }
}