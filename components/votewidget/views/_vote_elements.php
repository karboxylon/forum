<?php use app\models\interfaces\VoteInterface;

if($userVote === VoteInterface::TYPE_POST || $userVote === false): ?>
<a href="#" class="voting" style="text-decoration: none">
    <?php endif; ?>
    <span class="label label-success voting-span" data-forum-element-id="<?= $model->id ?>" data-forum-element-type="<?= $forumElementType ?>" data-vote-type="<?= VoteInterface::VOTE_UP ?>"><i class="glyphicon glyphicon-thumbs-up" aria-hidden="true"></i>  <?= $model->getCountedVotes(VoteInterface::VOTE_UP)?></span>
    <?php if($userVote === VoteInterface::TYPE_POST || $userVote === false): ?>
</a>
<?php endif; ?>

<?php if($userVote === VoteInterface::TYPE_THREAD || $userVote === false): ?>
<a href="#" class="voting" style="text-decoration: none">
    <?php endif; ?>
    <span class="label label-danger voting-span"  data-forum-element-id="<?= $model->id ?>" data-forum-element-type="<?= $forumElementType ?>"  data-vote-type="<?= VoteInterface::VOTE_DOWN ?>"><i class="glyphicon glyphicon-thumbs-down" aria-hidden="true"></i>  <?= $model->getCountedVotes(VoteInterface::VOTE_DOWN)?></span>
    <?php if($userVote === VoteInterface::TYPE_THREAD || $userVote === false): ?>
</a>
<?php endif; ?>