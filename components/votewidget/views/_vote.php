<?php

use yii\helpers\Html;
use \app\models\interfaces\VoteInterface;
use yii\helpers\Url;

?>

<div class="voting-div">
    <?= $this->render('_vote_elements', ['model' => $model, 'userVote' => $userVote, 'forumElementType' => $forumElementType]) ?>
</div>

<?php


$url = Url::to(['vote/vote']);
$this->registerJs(<<<JS
function vote(selector, url){
    var that = $(selector);
    var span = that.find(".voting-span");
    var type = span.data("vote-type");
    var forumElementId = span.data("forum-element-id");
    var forumElementType = span.data("forum-element-type");
    
   $.post(url, {forumElementId: forumElementId, type: type, forumElementType: forumElementType}, null, 'json')
        .fail(function(data) {
            console.log('Error ' + data + '.');
        })
        .done(function(data) {
            var parent = that.parent();
            $(parent).html(data.data);
        });
 }
JS
);


$this->registerJs(<<<JS
$(document).on('click', 'a.voting',function(e) {
    e.preventDefault();
    vote($(this), "$url");
 });
JS
);
