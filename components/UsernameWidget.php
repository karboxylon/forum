<?php

namespace app\components;

use app\models\db\User;
use yii\base\Widget;
use yii\helpers\Html;

class UsernameWidget extends Widget
{
    public $id;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $user = User::findOne(['id'=>$this->id]);
        return Html::encode($user->username);
    }
}