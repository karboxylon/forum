<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => "mysql:host=localhost;dbname=yii2forum;port=3306;",
    'username' => 'app_user',
    'password' => 'app_pass',
    'charset' => 'utf8',
];