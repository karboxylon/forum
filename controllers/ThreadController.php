<?php

namespace app\controllers;

use app\models\db\Post;
use app\models\db\UserThread;
use app\models\interfaces\VoteInterface;
use Yii;
use app\models\db\Thread;
use app\models\ThreadSearch;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\Cookie;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ThreadController implements the CRUD actions for Thread model.
 */
class ThreadController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['create', 'view', 'update', 'delete', 'index', 'vote'],
                        'roles' => ['@'],
                        'allow' => true
                    ],
                    [
                        'actions' => ['index', 'view'],
                        'roles' => ['?'],
                        'allow' => true
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Thread models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ThreadSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Thread model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $thread = $this->findModel($id);

        if($this->validToUpdateViewsCounter($id)){
            $thread->updateCounters(['views' => 1]);
        }

        $post = new Post();
        $post->author_id = Yii::$app->getUser()->id;
        $post->thread_id = $thread->id;

        if ($post->load(Yii::$app->request->post()) && $post->validate()) {
            $post->save();
            return $this->redirect(['view', 'id' => $thread->id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => Post::find()->where(['thread_id' => $thread->id])->orderBy('id DESC'),
        ]);

        return $this->render('view', [
            'model' => $thread,
            'postModel' => $post,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Creates a new Thread model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Thread();
        $newDatetime = new \DateTime();
        $model->creation_date = $newDatetime->format('Y/m/d H:i:s');
        $model->author_id = Yii::$app->getUser()->id;
        $model->views = 0;

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            $errors = $model->errors;
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Thread model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Thread model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Thread model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Thread the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Thread::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param int $threadId
     * @return bool
     */
    protected function validToUpdateViewsCounter($threadId)
    {
        $cookies = Yii::$app->request->cookies;
        $threadsViewed=[];

        if(isset($cookies['threads-viewed'])){
           $threadsViewed = json_decode($cookies['threads-viewed']);
        }

        if(!in_array($threadId, $threadsViewed)){
            array_push($threadsViewed, $threadId);
            $cookie = new Cookie([
                'name' => 'threads-viewed',
                'value' => json_encode( $threadsViewed),
                'expire' => strtotime('today 23:59')
            ]);

            \Yii::$app->response->cookies->add($cookie);
            return true;
        }

        return false;
    }
}
