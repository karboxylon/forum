<?php

namespace app\controllers;

use app\models\db\Post;
use app\models\db\Thread;
use app\models\db\UserPost;
use app\models\db\UserThread;
use app\models\interfaces\VoteInterface;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\Controller;

class VoteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['vote'],
                        'roles' => ['@'],
                        'allow' => true
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionVote()
    {
        if (!Yii::$app->request->isAjax) {
            return $this->redirect(['site/index']);
        }

        $data = [
            'error' => 1,
            'data' => Html::tag('span',
                Html::tag('span', '')
                . 'Error ' ,
                ['class' => 'text-danger']
            ),
        ];

        $forumElementId = Yii::$app->request->post('forumElementId');
        $voteType = Yii::$app->request->post('type');
        $forumElementType = Yii::$app->request->post('forumElementType');

        if($forumElementId && $forumElementType && in_array($voteType, [VoteInterface::VOTE_UP, VoteInterface::VOTE_DOWN])){
            $this->updateByForumElementType($forumElementId, $forumElementType, $voteType);

            $voteView = $this->renderViewByForumElementType($forumElementId, $forumElementType);


            $model = Thread::findOne(['id' => $forumElementId]);


            $data = [
                'error' => 0,
                'data' => $voteView
            ];
        }

        return Json::encode($data);

    }

    /**
     * @param int $forumElementId
     * @param int $forumElementType
     * @param int $voteType
     */
    protected function updateByForumElementType($forumElementId, $forumElementType, $voteType)
    {
        switch ($forumElementType) {
            case VoteInterface::TYPE_THREAD:
                $this->updateThreadVote($forumElementId, $forumElementType, $voteType);
                break;
            case VoteInterface::TYPE_POST:
                $this->updatePostVote($forumElementId, $forumElementType, $voteType);
                break;
        }
    }

    /**
     * @param int $forumElementId
     * @param int $forumElementType
     * @param int $voteType
     */
    protected function updateThreadVote($forumElementId, $forumElementType, $voteType)
    {
        $userId = Yii::$app->user->id;
        $userThread = UserThread::findOne(['thread_id' => $forumElementId, 'user_id'=> $userId]);

        if($userThread){
            $userThread->type = $voteType;
        }else{
            $userThread = new UserThread();
            $userThread->user_id = $userId;
            $userThread->thread_id = $forumElementId;
            $userThread->type = $voteType;
        }
        $userThread->save();
    }

    /**
     * @param int $forumElementId
     * @param int $forumElementType
     * @param int $voteType
     */
    protected function updatePostVote($forumElementId, $forumElementType, $voteType)
    {
        $userId = Yii::$app->user->id;
        $userPost = UserPost::findOne(['post_id' => $forumElementId, 'user_id'=> $userId]);

        if($userPost){
            $userPost->type = $voteType;
        }else{
            $userPost = new UserPost();
            $userPost->user_id = $userId;
            $userPost->post_id = $forumElementId;
            $userPost->type = $voteType;
        }
        $userPost->save();
    }

    /**
     * @param int $forumElementId
     * @param int $forumElementType
     * @return string
     */
    protected function renderViewByForumElementType($forumElementId, $forumElementType)
    {
        $model = null;
        switch ($forumElementType) {
            case VoteInterface::TYPE_THREAD:
                $model = Thread::findOne(['id' => $forumElementId]);
                break;
            case VoteInterface::TYPE_POST:
                $model = Post::findOne(['id' => $forumElementId]);
                break;
        }

        $userVote = $model->getUserVote(Yii::$app->user->id);

        return $this->renderAjax('/../components/votewidget/views/_vote_elements', ['model' => $model, 'userVote' => $userVote, 'forumElementType' => $forumElementType]);
    }

}
