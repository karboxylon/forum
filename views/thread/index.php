<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ThreadSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Threads';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="thread-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?php
        if (!Yii::$app->user->isGuest){
            echo Html::a('Create Thread', ['create'], ['class' => 'btn btn-success']);
        }
        ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'title',
            'content:ntext',
            'creation_date',
            'author_id',
             'views',
            [
                'attribute'=>'replies',
                'label'=>'Replies',
                'value'=>function($model){
                    return count($model->posts);
                }
            ],

            ['class' => 'yii\grid\ActionColumn',
                'template' => Yii::$app->user->isGuest ? '{view}' :  '{view} {update} {delete}'
            ],
        ],
    ]); ?>
</div>
