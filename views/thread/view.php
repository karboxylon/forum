<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
use yii\widgets\ListView;
use \app\components\votewidget\VoteWidget;
use \app\components\UsernameWidget;

/* @var $form ActiveForm */

/* @var $this yii\web\View */
/* @var $model app\models\db\Thread */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Threads', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="thread-view">
        <h1 class="pull-left"><?= Html::encode($this->title) ?></h1>
        <h3 class="pull-right">
            <?= VoteWidget::widget(['model' => $model, 'forumElementType' => \app\models\interfaces\VoteInterface::TYPE_THREAD]) ?>
        </h3>
        <div class="clearfix"></div>

<?php         if (!Yii::$app->user->isGuest) : ?>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
<?php endif; ?>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'title',
            'creation_date',
            'author_id',
            'views',
            'content:ntext'
        ],
    ]) ?>

</div>


<?= ListView::widget([
    'dataProvider' => $dataProvider,
    'itemOptions' => ['class' => 'item'],
    'itemView' => function ($modelPost, $key, $index, $widget) {

    echo '<div class="panel panel-default">  
                <div class="panel-heading"> 
                    <div class="panel-title pull-left">'.HTML::encode($modelPost->creation_date).' <span class="badge badge-success">' . UsernameWidget::widget(['id' => $modelPost->author_id]). '</span></div> 
                    <div class="panel-title pull-right">'.VoteWidget::widget(['model' => $modelPost, 'forumElementType' => \app\models\interfaces\VoteInterface::TYPE_POST]).'
                    </div>
                    <div class="clearfix"></div>
                </div>
                    <div class="panel-body"> '.HTML::encode($modelPost->content). '</div>
            </div>';
    },
    'layout' => "{items}\n{pager}",
]) ?>

<?php         if (!Yii::$app->user->isGuest) : ?>

<div class="post">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($postModel, 'content') ?>

    <div class="form-group">
        <?= Html::submitButton('Submit', ['class' => 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>

</div><!-- post -->

<?php endif; ?>

