<?php

namespace app\models\db;

use app\models\interfaces\VoteInterface;
use yii\db\ActiveRecord;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "post".
 *
 * @property integer $id
 * @property string $content
 * @property string $creation_date
 * @property integer $thread_id
 * @property integer $author_id
 *
 * @property User $author
 * @property Thread $thread
 */
class Post extends ActiveRecord implements VoteInterface
{
    public function init()
    {
        parent::init();

        $newDatetime = new \DateTime();
        $this->creation_date = $newDatetime->format('Y/m/d H:i:s');
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['content', 'string'],
            ['content', 'required'],
            ['content', 'filter', 'filter' => function ($value) {
                return HtmlPurifier::process(trim($value), [
                    'HTML.Allowed' => 'p,br,b,strong,ul,li,ol,hr',
                ]);
            }],
            [['creation_date'], 'safe'],
            [['thread_id', 'author_id'], 'required'],
            [['thread_id', 'author_id'], 'integer'],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['thread_id'], 'exist', 'skipOnError' => true, 'targetClass' => Thread::className(), 'targetAttribute' => ['thread_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => 'Content',
            'creation_date' => 'Creation Date',
            'thread_id' => 'Thread ID',
            'author_id' => 'Author ID',
        ];
    }

    /**
     * Author
     * @return ActiveRecord
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id'])->inverseOf('posts');
    }

    /**
     * Thread
     * @return ActiveRecord
     */
    public function getThread()
    {
        return $this->hasOne(Thread::className(), ['id' => 'thread_id'])->inverseOf('posts');
    }

    /**
     * @inheritdoc
     * @return PostQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PostQuery(get_called_class());
    }

    /**
     * @param int $type
     * @return int
     */
    public function getCountedVotes($type)
    {
        return count(UserPost::findAll(['post_id'=>$this->id, 'type' => $type]));
    }

    /**
     * @param int $userId
     * @return bool|int
     */
    public function getUserVote($userId)
    {
        $userVote = UserPost::findOne(['post_id'=>$this->id, 'user_id'=> $userId]);

        if($userVote){
            return $userVote->type;
        }

        return false;    }
}
