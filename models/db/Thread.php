<?php

namespace app\models\db;

use app\models\interfaces\VoteInterface;
use yii\db\ActiveRecord;
use yii\helpers\HtmlPurifier;

/**
 * This is the model class for table "thread".
 *
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property string $creation_date
 * @property integer $author_id
 * @property integer $views
 *
 * @property User $author
 */
class Thread extends ActiveRecord implements VoteInterface
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'thread';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['content', 'string'],
            ['content', 'required' ],
            ['content', 'filter', 'filter' => function ($value) {
                return HtmlPurifier::process(trim($value), [
                    'HTML.Allowed' => 'p,br,b,strong,ul,li,ol,hr',
                ]);
            }],
            [['creation_date'], 'safe'],
            [['author_id'], 'required'],
            [['author_id', 'views'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['title'], 'required'],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['author_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'content' => 'Content',
            'creation_date' => 'Creation Date',
            'author_id' => 'Author ID',
            'views' => 'Views',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'author_id'])->inverseOf('threads');
    }

    public function getPosts()
    {
        return $this->hasMany(Post::className(), ['thread_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return ThreadQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ThreadQuery(get_called_class());
    }

    /**
     * @return int
     */
    public function getReplies() {
        return Post::find()->where(['thread_id' => $this->id])->count();
    }


    /**
     * UserThread[]
     * @return ActiveRecord[] $userThreads
     */
    public function getThreadUsers() {
        return $this->hasMany(User::className(), ['id' => 'user_id'])
            ->viaTable('user_thread', ['thread_id' => 'id']);
    }

    /**
     * @param int $type
     * @return int
     */
    public function getCountedVotes($type)
    {
        return count(UserThread::findAll(['thread_id'=>$this->id, 'type' => $type]));
    }

    /**
     * @param int $userId
     * @return bool|int
     */
    public function getUserVote($userId)
    {
        $userVote = UserThread::findOne(['thread_id'=>$this->id, 'user_id'=> $userId]);

        if($userVote){
            return $userVote->type;
        }

        return false;
    }

}
