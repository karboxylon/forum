<?php

namespace app\models\forms;

use app\models\db\User;
use yii\base\Model;

class RegisterForm extends Model
{
    public $username;
    public $password;

    public $testBotField;
    /**
     * @var string
     */
    public $captcha;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['username', 'password'], 'required'],
            ['username', 'string', 'min' => '6'],
            ['username', function ($attribute, $params) {
                if(User::findOne(['username' => $this->$attribute])){
                    $this->addError($attribute, 'The username is not available.');
                }
            }],
            ['captcha', 'captcha'],
            ['testBotField',  function ($attribute, $params) {
                if(!is_null($this->$attribute)){
                    $this->addError($attribute, 'You are bot.');
                }
            }],
        ];

    }
}