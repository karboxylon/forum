<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\db\Thread;

/**
 * ThreadSearch represents the model behind the search form about `app\models\db\Thread`.
 */
class ThreadSearch extends Thread
{

    public $replies;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'author_id', 'views'], 'integer'],
            [['title', 'content', 'creation_date'], 'safe'],
            [['replies'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Thread::find();

        // add conditions that should always apply here
        $query->joinWith(['posts']);
        $query->select('thread.*, count(post.id) AS replies');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
                'attributes' => [
                    'creation_date',
                    'views',
                    'replies'
                ],
            ]
        );
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'creation_date' => $this->creation_date,
            'thread.author_id' => $this->author_id,
            'views' => $this->views,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'thread.content', $this->content]);

        $query->addGroupBy('thread.id');

        return $dataProvider;
    }
}
