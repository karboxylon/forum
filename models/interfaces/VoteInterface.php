<?php

namespace app\models\interfaces;


interface VoteInterface
{
    const VOTE_UP = 1;
    const VOTE_DOWN = 2;

    const TYPE_THREAD = 1;
    const TYPE_POST = 2;

    /**
     * @param int $type
     * @return int
     */
    public function getCountedVotes($type);

    /**
     * @param int $userId
     * @return bool|int
     */
    public function getUserVote($userId);
}