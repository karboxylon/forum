<?php

use yii\db\Migration;

/**
 * Handles adding auth_key to table `user`.
 */
class m170525_133709_add_auth_key_column_to_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'auth_key', $this->string());
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user', 'auth_key');
    }
}
