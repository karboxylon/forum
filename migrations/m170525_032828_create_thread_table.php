<?php

use yii\db\Migration;

/**
 * Handles the creation of table `thread`.
 * Has foreign keys to the tables:
 *
 * - `user`
 */
class m170525_032828_create_thread_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('thread', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'content' => $this->text(),
            'creation_date' => $this->dateTime(),
            'author_id' => $this->integer()->notNull(),
            'views' => $this->integer(),
        ]);

        // creates index for column `author_id`
        $this->createIndex(
            'idx-thread-author_id',
            'thread',
            'author_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-thread-author_id',
            'thread',
            'author_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-thread-author_id',
            'thread'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-thread-author_id',
            'thread'
        );

        $this->dropTable('thread');
    }
}
