<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_thread`.
 * Has foreign keys to the tables:
 *
 * - `user`
 * - `thread`
 */
class m170528_170311_create_junction_table_for_user_and_thread_tables extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_thread', [
            'user_id' => $this->integer(),
            'thread_id' => $this->integer(),
            'type' => $this->integer(),
            'PRIMARY KEY(user_id, thread_id)',
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            'idx-user_thread-user_id',
            'user_thread',
            'user_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-user_thread-user_id',
            'user_thread',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        // creates index for column `thread_id`
        $this->createIndex(
            'idx-user_thread-thread_id',
            'user_thread',
            'thread_id'
        );

        // add foreign key for table `thread`
        $this->addForeignKey(
            'fk-user_thread-thread_id',
            'user_thread',
            'thread_id',
            'thread',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        // drops foreign key for table `user`
        $this->dropForeignKey(
            'fk-user_thread-user_id',
            'user_thread'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            'idx-user_thread-user_id',
            'user_thread'
        );

        // drops foreign key for table `thread`
        $this->dropForeignKey(
            'fk-user_thread-thread_id',
            'user_thread'
        );

        // drops index for column `thread_id`
        $this->dropIndex(
            'idx-user_thread-thread_id',
            'user_thread'
        );

        $this->dropTable('user_thread');
    }
}
