# README #

First small app written with Yii2 Framework.

### What is this repository for? ###

* Technical test

### Features done: ###

Tasks:
- Database to hold the data (user, thread, post);
- Forms to add new Threads and Posts
- Validation in forms (max 100 characters and filtering URLs in content)

Optional:
- Login system for Users
- Sorting for threads (by views, replies, creation date)
